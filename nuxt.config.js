export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'creativeleadz1',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // '~/assets/css/owl.carousel.min.css',
    '~/assets/fonts/icomoon/style.css',
    '~/assets/css/bootstrap.min.css',
    '~/assets/css/style.css',
    '~/assets/css/mystyle.css'
  ],

  script:[
    
    {
      src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
      type: "text/javascript"
    },
    {
      src:
        "~/assets/js/mainJS.js",
      type: "text/javascript"
    },
    // { src: '~assets/js/jquery-3.3.1.min.js', body: true }
    // { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js' }
    // { src: '~/assets/js/mainJS.js', body: true }
    // { src: '~/assets/js/main.js' },
    // '~/assets/js/main.js'
    // { src: '~/assets/js/popper.min.js' },
    // { src: '~/assets/js/bootstrap.min.js' },
    // { src: '~/assets/js/jquery.sticky.js' },
    // { src: '~/assets/js/main.js' }
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/bootstrap.js'
    // { src: '~/assets/js/jquery-3.3.1.min.js', mode: 'client' },
    // { src: '~/assets/js/popper.min.js', mode: 'client' },
    // { src: '~/assets/js/bootstrap.min.js', mode: 'client' },
    // { src: '~/assets/js/jquery.sticky.js', mode: 'client' },
    // { src: '~/assets/js/main.js', mode: 'client' },
    
    // { src: '~/assets/js/mainJS.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}


